# Payworks_Point

ABOUT

This implements the PayWorks API.

The example PHP code available there implements only one method - 'payworks_point_one_time' -
this method makes payment via PayWorks processor. You can do one time payments for purchases from it.

Requirements - In order to use the api, you will be required to have a company accounts on PayWorks.
The api requires sending your private key as well as email address in the request envelope. 

So, here you go.

This code is released under the terms of the ASL2 (Apache Software
License, version 2).

Hosted at http://www.payworks.bs/paywork/payworks_point_one_time

INSTALLATION

Sample code is present at - https://www.payworks.bs/payworks-pay/sample-code/

Demo available at - https://www.payworks.bs/services/payworks-point.php

We are updating the sample-code folder for Python, ASP.net and other major technologies.

